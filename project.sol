pragma solidity ^0.4.16; 

contract administration {
    address public admin;
    
    constructor() public {
        admin = msg.sender;
    }

    modifier onlyAdmin {
        require(msg.sender == admin);
        _;
    }

    function transferAdminRole(address newAdmin) onlyAdmin public {
        admin = newAdmin;
    }
}

//Ecrire les modifier (véhicules - total etc... pour aussi montrer que la partie admin marche)
//modifier onlyOwner(address caller) {
        //require(caller == _owner, "You're not the owner of the contract");
        //_;
    //}

contract billContract is administration{
    
    struct Invoice {
        uint billNumber;
        string garage;
        string description;
        string plateNumber;
        uint total;
        
    }
    
    string[2] agregatedGarage;
    
   constructor () public {
       agregatedGarage[0] = "Fefe";
       agregatedGarage[1] = "Peugeot";
   }
    
    mapping (address => Invoice) invoices;
    mapping (address => uint) administrations;
    
    address[] public invoicesAccount;

    function CreateInvoice(uint _billNumber, string _garage ,string _description,string _plateNumber , uint _total) public {
        var invoice = invoices[tx.origin];

        invoice.billNumber = _billNumber;
        invoice.garage = _garage;
        invoice.description = _description;
        invoice.plateNumber = _plateNumber;
        invoice.total = _total;
        
        invoicesAccount.push(tx.origin) -1;
    }
    
    function getAdress() view public returns(address[]) {
        return invoicesAccount;
    }
    
    function getLastContract(address _address) view public returns (uint, string,string,string,uint) {
        return (invoices[_address].billNumber,invoices[_address].garage,invoices[_address].description,invoices[_address].plateNumber, invoices[_address].total);
    }
    
    function countIds() view public returns (uint) {
        return invoicesAccount.length;
    }
    
    function isReparable(address _address) view public returns (uint, string, uint, bool) {
        if ((invoices[_address].total) > 5000) {
            return (invoices[_address].billNumber,invoices[_address].plateNumber, invoices[_address].total, false);
        }
        else {
            return (invoices[_address].billNumber,invoices[_address].plateNumber, invoices[_address].total, true);
        }
    }
    
    function isAgregated(address _address) view public returns (uint, string, string, uint, bool) {
        if ((keccak256(invoices[_address].garage) ==  keccak256(agregatedGarage[0]))
        ||(keccak256(invoices[_address].garage) ==  keccak256(agregatedGarage[1]))) {
            
            return (invoices[_address].billNumber,invoices[_address].plateNumber,invoices[_address].garage, invoices[_address].total, true);
        }
        else {
            
            return (invoices[_address].billNumber,invoices[_address].plateNumber,invoices[_address].garage, invoices[_address].total, false);
        }
    }
    
    function isPlateNumberValid(address _address) view public returns (uint, string, bool) {
        if (bytes(invoices[_address].plateNumber).length != 9 ) {
            return (invoices[_address].billNumber,invoices[_address].plateNumber,false);
        }
        else {
            return (invoices[_address].billNumber,invoices[_address].plateNumber,true);
        }
    }
}