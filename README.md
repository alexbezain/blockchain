Our project consists in answering a real problem within the context of 'LaPoste'.
Indeed, when a postman has an incident with his vehicle, for example a problem in his vehicle’s engine. He takes the vehicle to the garage, approved by his manager. Then, the garage owner calls the manager to ensure if an agreement has been issued for this vehicle. 
When the agreement is issued, the mechanic starts the repair work. Then when it’s finished, the repairer fills the bill for the “La Poste” manager. The bill is further given to the postman who has to send it quickly to his manager in order to close the file. Finally, the bill comes to the manager, the data is recorded by hand on a computer and then processed and recorded in the ERP system. 

To run this project go to https://remix.ethereum.org
